Ultrasonic es un cliente de Subsonic (y servidores compatibles) para Android. Usa Ultrasonic para conectar con tu servidor y escuchar música.

Características principales:
* Ligero
* Rápido
* Tema claro y oscuro
* Soporte de múltiples servidores
* Modo fuera de línea
* Marcadores
* Listas de reproducción en el servidor
* Modo de reproducción aleatoria
* Modo Jukebox
* Chat del servidor
* Y muchas mas!!!

El código esta disponible con licencia GPL en GitLab: https://gitlab.com/ultrasonic/ultrasonic
Si tienes problemas puedes dejar tu petición en: https://gitlab.com/ultrasonic/ultrasonic/issues
Icono diseñado por Sebastien Gabriel: http://www.flaticon.com/authors/sebastien-gabriel
